import org.apache.zookeeper.*;
import org.apache.zookeeper.data.Stat;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.util.List;

public class testZookeeper {

    //连接zkserver
    private String connectString="hadoop01:2181,hadoop02:2181,hadoop03:2181,hadoop04:2181,hadoop04:2181";
    //超时时间
    private int sessionTimeout=2000;
    //监听器
    private Watcher watcher;
    private ZooKeeper zkclient;

    //初始化客户端
    @Before
    public void initClient() throws IOException {
        zkclient = new ZooKeeper(connectString, sessionTimeout, new Watcher() {

            private List<String> childrens;

            public void process(WatchedEvent watchedEvent) {
                System.out.println(watchedEvent.getType()+"\t"+watchedEvent.getPath());
                try {
                    childrens = zkclient.getChildren("/", true);
                    for (String child:childrens ) {
                        System.out.println(child);
                    }
                } catch (KeeperException e) {
                    e.printStackTrace();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    //创建子节点
    @Test
    public void createSubNode() throws KeeperException, InterruptedException {
        /**
         *
         * String path,  节点路径
         * byte[] data,  节点数据
         * List<ACL> acl, 访问规则
         * CreateMode createMode    节点模式
         */
        String path = zkclient.create("/createNode", "createNode".getBytes(), ZooDefs.Ids.OPEN_ACL_UNSAFE, CreateMode.PERSISTENT);
        System.out.println(path);

    }
    //获取子节点

    @Test
    public void getChild() throws KeeperException, InterruptedException {
        /**
         * path  节点路径
         * watch  对此节点路径，是否监听
         */
        List<String> childrens = zkclient.getChildren("/", true);
        for (String child:childrens ) {
            System.out.println(child);
        }
    }
    //判断节点是否存在
    @Test
    public void exist() throws KeeperException, InterruptedException {
        /**
         * path  节点路径
         * watch  是否添加监听
         */
        Stat exists = zkclient.exists("/createNode", false);
        System.out.println(exists==null?"节点不存在":"节点存在");

    }



















}
